<?php


namespace App\model;


use Symfony\Component\Validator\Constraints as Assert;

class EventModel
{
    /**
     * @var string|null
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string|null
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var string|null
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var \DateTime|null
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $start_at;

    /**
     * @var \DateTime|null
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $end_at;

    /**
     * @var int|null
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $user_id;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return EventModel
     */
    public function setTitle(?string $title): EventModel
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return EventModel
     */
    public function setContent(?string $content): EventModel
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return EventModel
     */
    public function setAddress(?string $address): EventModel
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartAt(): ?\DateTime
    {
        return $this->start_at;
    }

    /**
     * @param \DateTime|null $start_at
     * @return EventModel
     */
    public function setStartAt(?\DateTime $start_at): EventModel
    {
        $this->start_at = $start_at;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndAt(): ?\DateTime
    {
        return $this->end_at;
    }

    /**
     * @param \DateTime|null $end_at
     * @return EventModel
     */
    public function setEndAt(?\DateTime $end_at): EventModel
    {
        $this->end_at = $end_at;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * @param int|null $user_id
     * @return EventModel
     */
    public function setUserId(?int $user_id): EventModel
    {
        $this->user_id = $user_id;
        return $this;
    }




}