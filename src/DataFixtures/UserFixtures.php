<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //création role
        $role_admin = new Role();
        $role_admin->setLabel("ROLE_ADMIN");
        $role_user = new Role();
        $role_user->setLabel("ROLE_USER");
        $manager->persist($role_admin);
        $manager->persist($role_user);
        //fin
        //creation group
        $group_administrator = new Group();
        $group_administrator->setLabel("Administrator");
        //TODO verfier relation changer en role ?
        $group_administrator->addRelation($role_admin);
        $group_administrator->addRelation($role_user);
        $group_user = new Group();
        $group_user->setLabel("User");
        $group_user->addRelation($role_user);
        $manager->persist($group_administrator);
        $manager->persist($group_user);
        //fin
        //création d'un user
        $user = new User();
        $user
            ->setFirstName("User")
            ->setLastName("user")
            ->setAddress("user")
            ->setEmail("user@user.be")
            ->setPassword($this->encoder->encodePassword($user,"User@1234"))
            ->addGroup($group_user)
            ->addRole($role_user);

        $admin = new User();
        $admin
            ->setFirstName("Admin")
            ->setLastName("Admin")
            ->setAddress("Admin")
            ->setEmail("admin@admin.be")
            ->setPassword($this->encoder->encodePassword($admin,"Admin@1234"))
            ->addGroup($group_administrator)
            ->setRoles(['ROLE_ADMIN'])
            ->addRole($role_admin);

        $manager->persist($user);
        $manager->persist($admin);
        //fin
        $manager->flush();
    }
}
