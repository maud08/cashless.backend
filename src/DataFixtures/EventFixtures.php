<?php

namespace App\DataFixtures;

use App\DTO\EventDTO;
use App\Entity\Event;
use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class EventFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //création role
        $role_adminEvent = new Role();
        $role_adminEvent ->setLabel("ROLE_ADMINEVENT");
        $manager->persist($role_adminEvent);
        //fin
        //creation group
        $group_administrator = new Group();
        $group_administrator->setLabel("AdminEvent");
        $group_administrator->addRelation($role_adminEvent);
        $manager->persist($group_administrator);
        //fin

        $user = new User();
        $user
            ->setFirstName("adminEvent")
            ->setLastName("adminEvent")
            ->setAddress("admin")
            ->setEmail("adminEvent@user.be")
            ->setPassword($this->encoder->encodePassword($user,'adminEvent@1234'))
            ->addGroup($group_administrator)
            ->addRole($role_adminEvent);
        $manager->persist($user);
        $manager->flush();

       if($user){
           $event = new Event();
           $event
               ->setTitle("Mon super évenement")
               ->setContent("Etiam pharetra nisl ut gravida bibendum. Proin consequat
             ipsum purus, nec consequat erat pharetra sit amet. Integer pellentesque odio 
             sit amet consectetur imperdiet. In ipsum lacus, dignissim ac fringilla ullamcorper,
              vulputate id lacus. Vivamus imperdiet, orci non luctus tincidunt, massa nulla 
              lacinia urna, vel ornare tortor nibh vel ex. Ut eros nunc, accumsan nec diam mollis,
              lacinia gravida nibh. Pellentesque at sem et magna laoreet hendrerit ac non elit.
               Aliquam et tortor id erat rutrum laoreet vitae eget turpis.
               Nullam blandit dapibus dolor at condimentum.")
               ->setStartAt(new \DateTime("13-06-2020"))
               ->setEndAt(new \DateTime("20-06-2020"))
               ->setAddress("Route Baccara, 7090 Braine-le-Comte")
               ->setUser($user);
               $manager->persist($event);
           $manager->flush();
       }
    }

}
