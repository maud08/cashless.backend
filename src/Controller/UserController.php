<?php


namespace App\Controller;


use App\DTO\UserListDTO;
use App\DTO\UserProfileDTO;
use App\DTO\UserUpdateDTO;
use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use App\Form\GroupRegisterType;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

class UserController extends AbstractFOSRestController
{
    const GROUP_USER = "User";
    const ROLE_USER = "ROLE_USER";
    const GROUP_ADMIN = "Administrator";
    const ROLE_ADMIN = "ROLE_ADMIN";

    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }
//    /**
//     * @Rest\Get(path="/api/user/{id}")
//     * @REST\View()
//     */
//    public function getById($id)
//    {
//        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
//        return $user;
//    }
    /**
     * @Rest\Get(path="/api/profile/{email}")
     * @REST\View()
     */
    public function getByEmail(string $email)
    {
        $user = array($this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]));
        return array_map(function ($item){
            return new UserProfileDTO($item);
        },$user);
    }
    /**
     * @REST\Get(path="/api/user")
     * @REST\View()
     */
    public function getAllUser()
    {
        $list = $this->getDoctrine()->getRepository(User::class)->findAll();
        return array_map(function($item){
            return new UserListDTO($item);
        },$list);
    }

    /**
     * @Rest\Get(path="/api/user/{email}/roles")
     * @Rest\View()
     * @param string $email
     * @return array
     */
    public function getUserRole(string $email) {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["email" => $email]);

        return $user->getRoles();
    }
    /**
     * @Rest\Post(path="/api/inscription")
     * @Rest\View()
     */
    public function insert(Request $request, UserPasswordEncoderInterface $encoder)
    {
        //creation d'une nouvelle instance user
        $user = new User();
        //creation du formulaire se basant sur la class registerType
        $form = $this->createForm(RegisterType::class, $user,[
            // permet de désactiver le token de validation et autoriser angular de d'envoyer les info a symfony
            //on peut le désacitver globalement dans for_rest.yaml disable_csrf_role: ROLE_API
            "csrf_protection" => false
        ]);
        $json = $request->getContent();
        $data = json_decode($json,true);
        $form->submit($data);
        $form->handleRequest($request);
        //encodage du mot passe
        $hash = $encoder->encodePassword($user,$user->getPassword());
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            //récupération du mot passe encoder
            $user->setPassword($hash);
            $this->addDefaultGroup($user);
            $this->addDefaultRole($user);
            $em->persist($user);
            $em->flush();
            return new UserListDTO($user);
        }
        return $form;
    }

    /**
     * @REST\Put(path="/api/profile/{id}")
     * @REST\View()
     */
    public function Update(Request $request, UserPasswordEncoderInterface $encoder, $id)
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $user= $repo->find($id);
        $form = $this->createForm(RegisterType::class, $user,[
            "csrf_protection" => false
        ]);

        $json = $request->getContent();
        $data = json_decode($json,true);
        if($data["lastName"] == null )
            $data["lastName"] = $user->getLastName();
        if($data["firstName"] == null)
            $data["firstName"] = $user->getFirstName();
        if($data["address"] == null)
            $data["address"] = $user->getAddress();
        if($data["password"] == null)
            $data["password"] = $user->getPassword();
        if($data["email"] == null)
            $data["email"] = $user->getEmail();
        $form->submit($data);
        $form->handleRequest($request);
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $this->addDefaultGroup($user);
            $this->addDefaultRole($user);
            $em->persist($user);
            $em->flush();
            //TODO when security ok
//            $dto =  new UserUpdateDTO();
            //permet de créer le token
//            $dto->token = $manager->create($user);
//            $dto->user = new UserListDTO($user);

            return new UserListDTO($user);
        }
        return $form;
    }

    /**
     * @REST\Delete(path="/api/user/{id}")
     * @REST\View()
     */
    public function delete(Request $request, User $id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($id);
        $em->flush();
        return true;
    }

    /**
     * @REST\Put(path="/api/user/{user}")
     * @REST\View()
     */
    public function udpateGroup(Request $request, User $user)
    {

        $em = $this->getDoctrine()->getManager();
        $this->addAdminGroup($user);
        $this->addAdminRole($user);
        $em->persist($user);
        $em->flush();
        return new UserListDTO($user);
    }

    private function addDefaultGroup(User $user){
        $groupRepository = $this->manager->getRepository(Group::class);
        $user->addGroup($groupRepository->findOneBy(["label" => self::GROUP_USER]));
    }

    private function addDefaultRole(User $user){
        $roleRepository = $this->manager->getRepository(Role::class);
        $user->addRole($roleRepository->findOneBy(["label" => self::ROLE_USER]));
    }

    private function addAdminGroup(User $user){
        $groupRepository = $this->manager->getRepository(Group::class);
        $user->addGroup($groupRepository->findOneBy(["label" => self::GROUP_ADMIN]));
    }

    private function addAdminRole(User $user){
        $roleRepository = $this->manager->getRepository(Role::class);
        $user->addRole($roleRepository->findOneBy(["label" => self::ROLE_ADMIN]));
    }

}