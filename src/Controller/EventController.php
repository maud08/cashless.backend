<?php


namespace App\Controller;


use App\DTO\EventDTO;
use App\Entity\Event;
use App\Entity\User;
use App\Form\EventRegister;
use App\Form\RegisterType;
use App\model\EventModel;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;;

class EventController extends AbstractFOSRestController
{
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Rest\Get(path="/api/event")
     * @Rest\View()
     */
    public function getAll()
    {
        $list =$this->getDoctrine()->getRepository(Event::class)->findAll();
        return array_map(function ($item){
            return new EventDTO($item);
        },$list);
    }

    /**
     * @Rest\Get(path="/api/event/{id}")
     * @rest\View()
     * @param $id
     * @return array
     */
    public function getById($id)
    {
        $event = array($this->getDoctrine()->getRepository(Event::class)->find($id));
        return array_map(function ($item){
           return new EventDTO($item);
        }, $event);
    }

    /**
     * @Rest\Post(path="/api/event")
     * @Rest\View()
     * @param Request $request
     * @return EventDTO|array
     */
    public function insert(Request $request)
    {
        $model = new EventModel();
        $form = $this->createForm(EventRegister::class, $model,[
            "csrf_protection" => false
        ]);
        $json = $request->getContent();
        $data = json_decode($json, true);
        $form->submit($data);
        $form->handleRequest($request);
        if ($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $event = new Event();
            $event
                ->setTitle($model->getTitle())
                ->setContent($model->getContent())
                ->setAddress($model->getAddress())
                ->setStartAt($model->getStartAt())
                ->setEndAt($model->getEndAt());
            /** @var User $user */
               $user = $this->getDoctrine()->getRepository(User::class)->find($model->getUserId());
            if($user){
                $event->setUser($user);
                $em->persist($event);
                $em->flush();
                return new EventDTO($event);
            }
        }
        return ["message" => "Problème lors de l'insertion"];
    }

    /**
     * @Rest\Put(path="/api/event/{id}")
     * @Rest\View()
     * @param Request $request
     * @param $id
     * @return EventDTO|array
     */
    public function update(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository(Event::class);

        $model = new EventModel();
        $form = $this->createForm(EventRegister::class, $model,[
           "csrf_protection" => false
        ]);
        $json = $request->getContent();
        $data = json_decode($json,true);
        $form->submit($data);
        $form->handleRequest($request);
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $event = $repo->find($id);
            $event
                ->setTitle($model->getTitle())
                ->setContent($model->getContent())
                ->setAddress($model->getAddress())
                ->setStartAt($model->getStartAt())
                ->setEndAt($model->getEndAt());
            /** @var User $user */
               $user = $this->getDoctrine()->getRepository(User::class)->find($model->getUserId());
               if($user){
                   $event->setUser($user);
                   $em->persist($event);
                   $em->flush();
                   return new EventDTO($event);
               }

        }
        return ["message" => "Problème lors de l'udpate"];
    }
    /**
     * @Rest\Delete(path="/api/event/{id}")
     * @Rest\View()
     */
    public function delete(Request $request, Event $id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($id);
        $em->flush();
        return true;
    }
}
