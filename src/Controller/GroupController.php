<?php


namespace App\Controller;


use App\DTO\GroupDTO;
use App\Entity\Group;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

class GroupController extends AbstractFOSRestController
{
    public function __construct()
    {
    }

    /**
     * @Rest\Get(path="/api/group")
     * @Rest\View()
     */
    public function getAllGroup()
    {
        $list= $this->getDoctrine()->getRepository(Group::class)->findAll();
        return array_map(function($item){
            return new GroupDTO($item);
        },$list);
    }
}