<?php


namespace App\DTO;


use App\Entity\Group;

class GroupDTO
{
    /**
     * @var string
     */
    private $label;

    public function __construct(Group $group)
    {
        $this->label = $group->getLabel();
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return GroupDTO
     */
    public function setLabel(string $label): GroupDTO
    {
        $this->label= $label;
        return $this;
    }
}