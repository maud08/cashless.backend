<?php


namespace App\DTO;


class UserUpdateDTO
{
    public $token;
    public $user;
}