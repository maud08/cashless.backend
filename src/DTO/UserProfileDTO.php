<?php


namespace App\DTO;


use App\Entity\User;

class UserProfileDTO
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $email;


    public function __construct(User $user)
    {
        $this->id = $user->getId();
        $this->lastName = $user->getLastName();
        $this->firstName = $user->getFirstName();
        $this->address = $user->getAddress();
        $this->email = $user->getEmail();
    }

    /**
     * @param int $id
     * @return UserProfileDTO
     */
    public function setId(int $id): UserProfileDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $lastName
     * @return UserProfileDTO
     */
    public function setLastName(string $lastName): UserProfileDTO
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @param string $firstName
     * @return UserProfileDTO
     */
    public function setFirstName(string $firstName): UserProfileDTO
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @param string $address
     * @return UserProfileDTO
     */
    public function setAddress(string $address): UserProfileDTO
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @param string $email
     * @return UserProfileDTO
     */
    public function setEmail(string $email): UserProfileDTO
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->ide;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }




}