<?php
//va déterminé ce que l'on va envoyer lorsque j'appel la liste des user

namespace App\DTO;


use App\Entity\Group;
use App\Entity\User;

class UserListDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $group;


    //récupération des éléments de l'user appelé et construction de l'instance $user
    public function __construct(User $user)
    {
        $this->id = $user->getId();
        $this->lastName = $user->getLastName();
        $this->firstName = $user->getFirstName();
        $this->email = $user->getEmail();
        $this->address = $user->getAddress();
        $this->group = [];
        foreach ($user->getGroups() as $groupe) {
            $this->group[] = $groupe->getLabel();
        }

    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserListDTO
     */
    public function setId(int $id): UserListDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return UserListDTO
     */
    public function setLastName(string $lastName): UserListDTO
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return UserListDTO
     */
    public function setFirstName(string $firstName): UserListDTO
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return UserListDTO
     */
    public function setAddress(string $address): UserListDTO
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return UserListDTO
     */
    public function setEmail(string $email): UserListDTO
    {
        $this->email = $email;
        return $this;
    }
    /**
     * @param string $group
     * @return UserListDTO
     */
    public function setGroup(string $group): UserProfileDTO
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

}