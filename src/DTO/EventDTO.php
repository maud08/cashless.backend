<?php


namespace App\DTO;



use App\Entity\Event;

class EventDTO
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $start_at;

    /**
     * @var string
     */
    private $end_at;

    /**
     * @var int
     */
    private $user_id;

    public function __construct(Event $event)
    {
        $this->id = $event->getId();
        $this->title = $event->getTitle();
        $this->content = $event->getContent();
        $this->address = $event->getAddress();
        $this->start_at = $event->getStartAt();
        $this->end_at = $event->getEndAt();
        $this->user_id = (new UserListDTO($event->getUser()))->getId();
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return EventDTO
     */
    public function setUserId(int $user_id): EventDTO
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @param string $title
     * @return EventDTO
     */
    public function setTitle(string $title): EventDTO
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $content
     * @return EventDTO
     */
    public function setContent(string $content): EventDTO
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $start_at
     * @return EventDTO
     */
    public function setStartAt(string $start_at): EventDTO
    {
        $this->start_at = $start_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartAt(): string
    {
        return $this->start_at;
    }

    /**
     * @param string $end_at
     * @return EventDTO
     */
    public function setEndAt(string $end_at): EventDTO
    {
        $this->end_at = $end_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndAt(): string
    {
        return $this->end_at;
    }

    /**
     * @param string $address
     * @return EventDTO
     */
    public function setAddress(string $address): EventDTO
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }


}