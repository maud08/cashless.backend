<?php


namespace App\Form;


use App\Entity\Event;
use App\model\EventModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventRegister extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class)
            ->add('content', TextType::class)
            ->add('start_at', DateType::class, ["widget" => "single_text","format" => "yyyy-MM-dd","html5" => false, "required" => true])
            ->add('end_at', DateType::class, ["widget" => "single_text","format" => "yyyy-MM-dd","html5" => false, "required" => true])
            ->add('address', TextType::class)
            ->add('user_id', IntegerType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => EventModel::class
        ]);
    }
}